//
//  ViewController.swift
//  testembed
//
//  Created by J Mat Trang on 21/10/2016.
//  Copyright © 2016 ehda. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
 
    @IBOutlet weak var imageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        if Reachability.isConnectedToNetwork() == true {
            print("Internet connection OK")
            let alert = UIAlertView(title: "You are online", message: "All good.", delegate: nil, cancelButtonTitle: "OK")
            alert.show()
        } else {
            print("Internet connection FAILED")
            let alert = UIAlertView(title: "No Internet Connection", message: "Make sure your device is connected to the internet.", delegate: nil, cancelButtonTitle: "OK")
            alert.show()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}




 
