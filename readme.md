# Reachability Swift 3 xcode 8

Reachability was originally a C lib to check if device is connected to the internet. There's a way to get the C lib in swift too but there's a (simpler) pure Swift way too.
This is how to implement Reachability in Swift 3 that compiles in xcode 8.

* Create a swift file Reachability.swift in project dir

```

import Foundation
import SystemConfiguration


public class Reachability {
    
    class func isConnectedToNetwork() -> Bool {
        
        var zeroAddress = sockaddr_in(sin_len: 0, sin_family: 0, sin_port: 0, sin_addr: in_addr(s_addr: 0), sin_zero: (0, 0, 0, 0, 0, 0, 0, 0))
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
 guard let defaultRouteReachability = withUnsafePointer(to: &zeroAddress, {
 
 $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
 
 SCNetworkReachabilityCreateWithAddress(nil, $0)
 
 }
 
 }) else {
 
 return false
 }
 
        var flags: SCNetworkReachabilityFlags = SCNetworkReachabilityFlags(rawValue: 0)
        if SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) == false {
            return false
        }
        
        let isReachable = flags == .reachable
        let needsConnection = flags == .connectionRequired
        
        return isReachable && !needsConnection
        
    }
}
 

```


And call it in ViewController.swift, here with and image view demo that makes an alert if device is online and another one if device's off:

```

import UIKit

class ViewController: UIViewController {
 
    @IBOutlet weak var imageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        if Reachability.isConnectedToNetwork() == true {
            print("Internet connection OK")
            let alert = UIAlertView(title: "You are online", message: "All good.", delegate: nil, cancelButtonTitle: "OK")
            alert.show()
        } else {
            print("Internet connection FAILED")
            let alert = UIAlertView(title: "No Internet Connection", message: "Make sure your device is connected to the internet.", delegate: nil, cancelButtonTitle: "OK")
            alert.show()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}
```


